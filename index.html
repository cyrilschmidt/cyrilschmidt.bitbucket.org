<html>
<head>
<title>Conveyor</title>

<style>
body {counter-reset: h2; background-color: FFFBFB }
h2 {counter-reset: h3}
h2:before {counter-increment: h2; content: counter(h2) ". "}
h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
</style>

</head>
<body bgcolor="FFFBFB">
<h1>Conveyor</h1>
<p>This page describes <a href="https://bitbucket.org/cyrilschmidt/conveyor">Conveyor</a>, which is a concurrency framework for C++.</p>

<p><mark><strong>Note:</strong> at the moment of writing this (1 March 2015), the code is alpha-quality, maybe fit for experimeting with it, but by no means for real use.</mark></p>

<h2>Motivation</h2>
<p>In the XX century, multithreaded programs usually controlled access to shared resources by locking them with mutexes or semaphores. These techniques suffer from two well-known maladies: first, they make it difficult to write correct software; second, they are inefficient on modern hardware. A better approach is to allow each resource to be accessed by a single thread; when the thread finishes its operation, it passes the result to the next thread via a message queue. This is also not ideal; the drawbacks of this method were analysed by the team of LMAX and motivated them to create Disruptor. A description of Disruptor as well as explanation why it is much better than simple message passing can be found in <a href="#1">[1]</a>, <a href="#2">[2]</a> and <a href="#3">[3]</a>.</p>

<p>To recap the most important points:</p>
<ol type="1">
<li>Not only locks are slow. Atomic operations (such as compare and swap) where 2 threads take turns to write to the same memory location, are also much slower than one could expect (I repeated the measurements done by LMAX and got similar results, see <a href="#4">[4]</a>).</li>
<li>Any real bounded queue is usually in one of two states: empty or full.</li>
</ol>

<p>Consider 3 threads: A, B and C that communicate via queues. Thread A completes its piece of work and sends the result to queue 1. Thread B retrieves the message from queue 1, processes it and posts the result into queue 2. Thread C, in turn, takes the message from queue 2 and continues.
</p>

<img src="normal-queues.svg"/>

<p>The breakthrough of the Disruptor design is the idea to merge queue 1 and queue 2 into a single queue. Thread B will maintain a pointer to the entry that it operates upon. Once thread B is done, it moves the pointer to the next item. Thread C checks B's pointer and sees which entry is ready to be processed. Disruptor uses a bounded circular buffer: the entries that were processed by the last thread in the chain can be reused by the first thread (producer) to put new messages into.</p>

<img src="disruptor-buffer.svg"/>

<p>This design avoids write contention and does not require dynamic allocation of memory for the queue elements.</p>

<p>The original Disruptor is written in Java; later it was implemented in C++ <a href="#5">[5]</a>. I made another implementation of Disruptor in C++ trying to code it in a way that facilitates compiler optimisations. One option is to use static polymorphism <a href="#6">[6]</a>, that is, implement polymorphism by using templates rather than virtual functions. When an invocation of template function is being compiled, the compiler usually has access to the function's body. This allows the compiler to perform better optimisations, such as inlining the function body and eliminating common subexpressions across function call boundaries. It is by no means guaranteed that any particular compiler will use this opportunity, but giving more information to the compiler is almost always a good idea. A well-known drawback of static polymorphism is long compile time; it seems to be a reasonable tradeoff for runtime speed.</p>

<h2>Where the name comes from</h2>

<p>The setup where each worker thread maintains a pointer to the common buffer of tasks is not a queue anymore, but something that works like a production line <a href="#7">[7]</a>. Using the industrial analogy, it would be appropriate to call it <em>Conveyor</em>. The circular buffer that contains the units of work is called <em>belt</em>; the agents that operate on it are <em>workers</em>.</p>

<p>Probably the most famous example of a production line is the assembly line installed at Ford's
car factory <a href="#8">[8]</a>. Ironically, Ford's <em>assembly</em> line was inspired by <em>disassembly</em> lines at Chicago's butcheries, were workers in turn cut pieces off a cow's cadaver.</p>

<h2>Where it can be used</h2>

<p>Conveyor is designed for an application that uses a fixed number of CPU cores. To utilise all of them, the handling of each incoming request is split into a number of steps and each step is executed by a different CPU core. For instance, if we have 3 cores at our disposal, we would divide the processing into 3 steps and have those steps performed by 3 individual threads. When deciding where to cut the processing into chunks, we need to keep in mind that:</p>
<ul>
<li>different threads should not write to the same data structure;</li>
<li>when a thread has no work to do, it does not block but runs a busy waiting loop, continually checking for a new message to appear in the queue.</li>
</ul>

<p>The latter might seem a waste of CPU cycles, but it is not: if there is no work to do, it does not matter if the thread blocks or loops.</p>

<h2 id="Composing_operations">Composing operations</h2>

<p>Conveyor supports two ways of composing operations:</p>
<ol type="1">
<li>one operation starts after another;</li>
<li>two operations run in parallel.</li>
</ol>

<p>Some examples of compositions are shown graphically here:</p>

<ol type="1">
<li>Linear sequence
<br/>
<img src="linear-sequence.svg"/>
<br/>
Once worker A has finished processing an element of the belt, B starts processing it. Once B finishes, C starts.</li>
<p/>
<li>Diamond
<br/>
<img src="diamond.svg"/>
<br/>
Once A has finished, B and C run in parallel: that means, elements of the belt are processed by both B and C, in any order. Once both C and C finish, D starts.</li>
<p/>
<li>Asymmetric fork
<br/>
<img src="asymmetric-fork.svg"/>
<br/>
Once worker A has finished processing an element, both B and D can start working on it. C starts processing the element after B finishes with it.</li>
</ol>

<h2>Producer</h2>

<p>The first worker (in all examples above this is worker A) is special: it does not get a belt element as input. Its role is to put new elements on the belt. These elements will be used by subsequent workers as input. </p>

<h2 id="tying">Tying different workers to the same thread</h2>

<p>Sometimes a certain data item needs to be updated in two different steps, say step K and step N. We would like to stick to the rule "each data item is updated by one thread only", so we need to make sure that steps K and N are performed in the context of the same thread. Conveyor allows us to "tie" several workers to a certain thread; this feature will be presented in more detail <a href="#tying_notation">later</a>.</p>

<h2>How to use it</h2>

<p>Writing code that uses Conveyor is typically done in this order:</p>

<ol type="1">
<li>Specify the processing steps.</li>
<li>Define the elements of the belt.</li>
<li>Create the belt.</li>
<li>Instantiate workers.</li>
<li>Specify the order of execution.</li>
<li>Run the belt.</li>
</ol>
<p>We will present an example of it after the overview of Conveyor's API.</p>

<h2>Conveyor classes and operations</h2>

<p>The Conveyor API resides in the namespace <code>conveyor</code> and is declared in <code>conveyor.hpp</code> header file.</p>

<h3><code>belt</code></h3>

<p><code>template&lt;size_t length, typename sequence&gt; class belt;</code></p>

<p><code>belt</code> is the circular buffer that the workers operate on. Parameter <code>length</code> specifies the size of the buffer; it must be a power of two. <code>sequence</code> defines the types of the data members of each element of the belt. <code>sequence</code> is a model of the Forward Sequence concept of Boost MPL <a href="#8">[8]</a>.</p>

<p>The belt element is the only way for workers to communicate with each other, so its data members must convey all information that needs to pass between workers. Furthermore, each data member of the belt element should be written to by one thread only, while all data members can be read by all threads. The latter, however, is not enforced by Conveyor; it is the programmer's responsibility to follow that guideline.</p>

<h3><code>template&lt;typename belt&gt; belt_iterator</code></h3>

<p><code>belt_iterator</code> points to an element of the respective belt. A worker that reads or writes to the belt element dereferences an iterator and passes the result to the get function which retrieves the requested data member. An example will follow shortly.</p>

<h3><code>get&lt;size_t index, typename iterator_value&gt;(iterator_value& i)</code></h3>

<p><code>get</code> is a free function that retrieves a member from a dereferenced iterator passed to it. In a way get is similar to std::get function that retrieves an element from a tuple. Template parameter index specifies which member to retrieve. The return type of <code>get&lt;i&gt;</code> is the i-th element of the sequence that was passed as argument to belt.</p>

<p>For example, if we have</p>

<p><code>
typedef boost::mpl::vector&lt;int, long, double&gt; sequence;<br/>
conveyor::belt&lt;32, sequence&gt; my_belt;<br/>
my_belt::iterator iter;
</code></p>

<p>then the return types of get will be</p>
<p><code>
get&lt;0&gt;(*iter)	int&<br/>
get&lt;1&gt;(*iter)	long&<br/>
get&lt;2&gt;(*iter)	double&<br/>
</code></p>

<h3><code>template&lt;typename handler&gt; worker(handler h)</code></h3>

<p><code>worker</code> is a free function that returns a worker object which can operate on a belt. The handler argument is a function object which is invoked every time a new belt element becomes available. If the worker will operate on the belt of type my_belt, then the type of handler should be</p>

<p><code>size_t handler(my_belt::iterator begin, my_belt::iterator end);</code></p>

<p>The function is passed an iterator range. At that moment all belt elements within this range are ready to be processed by this worker. The function is allowed to process fewer than <code>(end - begin)</code> number of elements. Its return value must be equal to the number of processed elements: returned value n means that all elements in range <code>[begin, begin+n)</code> have been processed.</p>

<p>A typical handler function looks like</p>

<p><code>
auto handler(my_belt::iterator begin, my_belt::iterator end) -> decltype (end - begin) {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;auto iter = begin;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;for(;iter != end; ++iter) {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// retrieve input data<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int x = get&lt;A1&gt;(*iter);<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int y = get&lt;A2&gt;(*iter);<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// process the data<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;int result = x + y;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// save the result<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;get&lt;R&gt; = result;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;}<br/>
&nbsp;&nbsp;&nbsp;&nbsp;return iter - begin;<br/>
}
</code></p>

<p>Here, A1 and A2 are indices of the arguments; R is the index of the result in the belt element.<p>


<h3>Producer</h3>

<p>A producer is created by the same function with one important difference: its handler must have the type</p>

<p><code>std::tuple&lt;size_t, bool&gt; handler(my_belt::iterator begin, my_belt::iterator end);</code></p>

<p>The additional bool value indicates the end of processing. If the returned value is <code>&lt;x, false&gt;</code>, then the conveyor stops invoking the producer. It allows all created elements to be processed and terminates. This mechanism enables the producer to indicate that all work has been done.</p>

<h3 id="tying_notation">Tying different workers to the same thread</h3>

<p>There is a second form of the worker function:</p>

<p><code>template&lt;size_t id, typename handler&gt; conveyor::worker(handler h);</code></p>

<p>This function creates a worker which is tied to a thread with numerical identifier <code>id</code>. It is guaranteed that all workers tied to the same <code>id</code> will be invoked in the context of the same thread.</p>

<h3>Composing workers</h3>

<p>We already discussed sequential and parallel execution of workers in section <a href="#Composing_operations">4</a>; in this section we present the notation used to define the composition.</p>

<p>Operator <code>&gt;&gt;</code> specifies the sequential execution of its operands; operator <code>+</code> specifies the parallel execution. The examples from section <a href="#Composing_operations">4</a> can be written as follows:</p>

<ol type="1">
<li>Linear sequence
<br/>
<code>a &gt;&gt; b &gt;&gt; c;</code></li>
<p/>
<li>Diamond
<br/>
<code>a &gt;&gt; (b + c) &gt;&gt; d;</code></li>
<p/>
<li>Asymmetric fork
<br/>
<code>a &gt;&gt; ((b &gt;&gt; c) + d);</code></li>
</ol>
<p>Note: parenthesis in (2) is redundant, as operator <code>+</code> has a higher priority than operator <code>&gt;&gt;</code>. The parenthesis is there only for a human reader's benefit. In (3) the parenthesis around <code>b &gt;&gt; c</code> is essential.</p>

<h3><code>apply</code></h3>

<p>The <code>apply</code> function is a shorthand for creating a handler function. <code>apply</code> takes an ordinary function or function object and converts it to a handler that can be passed to <code>worker</code>. For example, the handler function from the example in section 8.4 can be defined as follows:</p>

<p><code>
auto handler = conveyor::apply&lt;A1,A2,R&gt;([](int x, int y) { return x+y; });
</p></code>

<h2>Example</h2>

<p>As an example, we develop a simplified version of grep; our application will read <code>stdin</code>line by line and print the lines that match a given regular expression.</p>
<p><mark><strong>Note:</strong> this example will not work with g++ earlier than version 4.9 because regex library is not fully supported by the pre-4.9 g++.</mark></p>

<p>We divide processing into three steps:</p>

<ol type="1">
<li>reading a line from <code>stdin</code>;</li>
<li>checking if the regular expression matches the line;</li>
<li>if a match is found, print it.</li>
</ol>

<p>What data do we need to pass from one worker to another? Step 1 passes a line of input to step 2. Step 2 passes a boolean flag to step 3; the flag is true is the corresponding line should be printed. We define our belt and its iterator as</p>

<p><code>typedef conveyor::belt&lt;128, boost::mpl::vector&lt;std::string, bool&gt;&gt; conv_belt;<br/>
typedef conveyor::belt_iterator&lt;conv_belt&gt;::iterator iter;
</code></p>

<p>where 128 is the length of the belt.</p>

<p>Before we start reading, we check that the pattern is provided to the application as the first (and only) command line argument:</p>

<p><code>
if (argc != 2) {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;std::cerr &lt;&lt; "Usage: " &lt;&lt; argv[0] &lt;&lt; " &lt;pattern&gt;\n";<br/>
&nbsp;&nbsp;&nbsp;&nbsp;return 1;<br/>
}<br/>
</code></p>

<p>Once we are sure the pattern is provided, we turn it into a regular expression object:</p>

<p><code>
std::regex regex(argv[1]);
</code></p>

The first worker is the producer that reads the standard input and puts its contents, line by line, on the belt, until it reaches the end of input:

<p><code>
auto w1 = worker([](iter i1, iter i2) {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;if(std::getline(std::cin, get&lt;0&gt;(*i1)))<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return std::make_tuple(1, true);<br/>
&nbsp;&nbsp;&nbsp;&nbsp;else<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return std::make_tuple(0, false);<br/>
});
</code></p>

<p>Having reached the end of file, the producer does not put anything on the belt and returns <code>(0, false)</code>. The conveyor will stop once all workers have processed the belt items up to this point.</p>

<p>The second worker matches the line of input to the regular expression and puts the result on the belt:</p>

<p><code>
auto w2 = worker(apply<iter, 0, 1>([&](const string& line) {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return std::regex_search(line, regex);<br/>
&nbsp;&nbsp;&nbsp;&nbsp;}));<br/>
</code></p>

<p>The last worker checks the boolean flag and prints the line if it is set:</p>

<p><code>
auto w3 = worker([](iter i1, iter i2) {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;if(get&lt;1&gt;(*i1))<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;std::cout &lt;&lt; get&lt;0&gt;(*i1) &lt;&lt; std::endl;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;return 1;<br/>
});<br/>
</code></p>

<p>Next, we define the belt and run the workers in the right sequence:</p>

<p><code>
std::regex regex(argv[1]);
b.run(w1 &gt;&gt; w2 &gt;&gt; w3);
</code></p>

The complete code is provided in <a href="https://bitbucket.org/cyrilschmidt/conveyor/src/HEAD/sgrep.cpp?at=master">sgrep.cpp</a>; building instructions can be found in <a href="https://bitbucket.org/cyrilschmidt/conveyor/src/HEAD/README.txt?at=master">README.txt</a>.

<h2>References</h2>

<ol type="1">
<li id="1"><a href="http://disruptor.googlecode.com/files/Disruptor-1.0.pdf">http://disruptor.googlecode.com/files/Disruptor-1.0.pdf</a></li>
<li id="2"><a href="http://martinfowler.com/articles/lmax.html">http://martinfowler.com/articles/lmax.html</a></li>
<li id="3"><a href="http://mechanitis.blogspot.nl/2011/06/dissecting-disruptor-whats-so-special.html">http://mechanitis.blogspot.nl/2011/06/dissecting-disruptor-whats-so-special.html</a></li>
<li id="4"><a href="http://mechanitis.blogspot.nl/2011/07/dissecting-disruptor-why-its-so-fast.html">http://mechanitis.blogspot.nl/2011/07/dissecting-disruptor-why-its-so-fast.html</a></li>
<li id="5"><a href="https://github.com/fsaintjacques/disruptor--">https://github.com/fsaintjacques/disruptor--</a></li>
<li id="6"><a href="http://en.wikipedia.org/wiki/Template_metaprogramming">http://en.wikipedia.org/wiki/Template_metaprogramming</a></li>
<li id="7"><a href="http://en.wikipedia.org/wiki/Production_line">http://en.wikipedia.org/wiki/Production_line</a></li>
<li id="8"><a href="http://en.wikipedia.org/wiki/Assembly_line#20th_century">http://en.wikipedia.org/wiki/Assembly_line#20th_century</a></li>
<li id="9"><a href="http://www.boost.org/doc/libs/release/libs/mpl/doc/refmanual/forward-sequence.html">http://www.boost.org/doc/libs/release/libs/mpl/doc/refmanual/forward-sequence.html</a></li>
</ol>

<div id="disqus_thread"></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = 'conveyor';
    
    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

</body>
</html>
